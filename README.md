# Android BioKeyboard #

Custom Android keyboard that captures touchscreen keystrokes from the user and is able to transmit the data back to a server.

## Copyright

Permission is granted to use this code in any way as long as the following publications are acknowledged:

1. Monaco, John V., et al. "Developing a keystroke biometric system for continual authentication of computer users." Intelligence and Security Informatics Conference (EISIC), 2012 European. IEEE, 2012.
2. Monaco, John V., et al. "Recent Advances in the Development of a Long-Text-Input Keystroke Biometric Authentication System for Arbitrary Text Input." Intelligence and Security Informatics Conference (EISIC), 2013 European. IEEE, 2013.

The authors will not be held liable for any damages that occur as a result of using this code.