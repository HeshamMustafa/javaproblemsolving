package com.vmonaco.mbbl;

import java.util.List;

import com.vmonaco.mbbl.events.BioEvent;

public interface BioEventConsumer {
	void onEventsReceived(Buffer buffer, List<? extends BioEvent> events);
	void onSessionEnd(Buffer buffer);
}
